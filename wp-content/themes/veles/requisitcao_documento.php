<?php
	// Template Name: Requisição de Atestados
?>
<?php get_header(); ?>
	<div class="container">
		<div class="span-24">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 
        <?php the_content(); ?>    
        <?php endwhile;  ?> 
        <?php endif; ?>
        <div class="container-requisicao">
            <h3> Formulário de Requisição </h3>

            <form id="form-requisicao" action="javascript:alert('Foi Enviado!');">
                <div id="note"></div>
                <div class="span-10 form notopmargin">
                    <label for="nome">Nome:</label>
                    <input type="text" name="nome" id="nome" placeholder="Nome" required />
                </div>

                <div class="span-4 form notopmargin">
                    <label for="ra">R.A.:</label>
                    <input type="text" name="ra" id="ra" placeholder="RA" required />
                </div>

                <div class="span-4 form notopmargin">
                    <label for="curso">Curso:</label>
                    <select name="curso" id="curso" required>
                        <option>Agronegócio</option>
                        <option>ADS</option>
                        <option>Comércio Exterior</option>
                    </select>
                </div>

                <div class="span-4 form notopmargin last">
                    <label for="turno">Turno:</label>
                    <select name="turno" id="turno" required>
                        <option>Manhã</option>
                        <option>Tarde</option>
                        <option>Noite</option>
                    </select>   
                </div>

                <div class="span-24 last">
                    <label for="documento">Documento:</label>
                    <select name="documento" id="documento" required>              
                        <option>Atestado de Matrícula</option>
                        <option>Atestado de Matrícula com Horário</option>
                        <option>Atestado de Frequência</option>
                        <option>Atestado de Realização de Prova (para estágio)</option>
                        <option>Atestado com Portaria de Reconhecimento do Curso</option>
                        <option>Histórico Escolar</option>
                        <option>Histórico Escolar com PR (Percentual de Rendimento)</option>                          
                    </select>   
                </div>

                <div class="span-24 last">
                    <label for="justificativa">Fundamente abaixo o objeto do requerimento:</label>
                    <textarea class="text" name="justificativa" id="justificativa" rows=5 cols=25 placeholder="Justificativa" required></textarea>
                </div>

                <div class="span-12">
                    <input type="submit" id="submit" value="Enviar" />
                    <input type="reset" id="reset" value="Apagar Valores" />
                </div>
            </form>
        </div>
        </div>
        <div class="clear"></div>
	</div>
<?php get_footer(); ?>