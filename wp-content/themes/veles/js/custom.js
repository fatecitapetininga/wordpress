
/***************************************************
			MASONRY (INDEX)
***************************************************/

jQuery.noConflict()(function($) {
  $(window).load(function(){
	$('.noticias-container').find('.span-24').masonry({
	    itemSelector : '.blog-index'
	});  	
  });
});


/***************************************************
			PRETTY PHOTO
***************************************************/

jQuery.noConflict()(function($){
$(document).ready(function() {  

$("a[rel^='prettyPhoto']").prettyPhoto({opacity:0.80,default_width:200,default_height:344,theme:'pp_default',hideflash:false,modal:false, social_tools:""});

});
});

/*** Transforms galleries into slideshow via 'prettyPhoto' ***/

jQuery.noConflict()(function($) {
	var galleries = $("div.gallery"),
		ids = [];

	galleries.each(function() {
		ids.push(this.id);
	})

	$(ids).each(function( index, value ) {
		$('div#' + value).find('a').attr('rel', 'prettyPhoto[' + value + ']');
	});

});

/***************************************************
			SuperFish Menu
***************************************************/	
// initialise plugins
	jQuery.noConflict()(function(){
		jQuery('ul.menu').superfish();
	});
	
	
	
jQuery.noConflict()(function($) {
  if ($.browser.msie && $.browser.version.substr(0,1)<7)
  {
	$('li').has('ul').mouseover(function(){
		$(this).children('ul').css('visibility','visible');
		}).mouseout(function(){
		$(this).children('ul').css('visibility','hidden');
		})
  }
}); 

jQuery.noConflict()(function(){
	// Setup Slider
	var aslider = jQuery(".my_asyncslider");
	
	aslider.asyncSlider({autoswitch: 5 * 1000, slidesNav: jQuery("#slideshow_nav")})
	/*** Pauses the slideshow on mouse hover ***/
	jQuery(aslider).hover(function() 	{
		aslider.asyncSlider("set", "autoswitch", "pause");
	},

	function() 	{
		aslider.asyncSlider("set", "autoswitch", "play");
	});	

});



jQuery.noConflict()(function($){
  $(document).ready(function(){
		$('#login-trigger').click(function(){
			$(this).next('#login-content').slideToggle();
			$(this).toggleClass('active');					
			
			if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
				else $(this).find('span').html('&#x25BC;')
			})
  });
});


jQuery.noConflict()(function($){
$(document).ready(function() {
	$('ul#filter a').click(function() {
		$(this).css('outline','none');
		$('ul#filter .current').removeClass('current');
		$(this).parent().addClass('current');
		
		var filterVal = $(this).text().toLowerCase().replace(' ','-');
				
		if(filterVal == 'all') {
			$('ul#portfolio li.hidden').fadeIn('slow').removeClass('hidden');
		} else {
			
			$('ul#portfolio li').each(function() {
				if(!$(this).hasClass(filterVal)) {
					$(this).fadeOut('normal').addClass('hidden');
				} else {
					$(this).fadeIn('slow').removeClass('hidden');
				}
			});
		}

		return false;
	});
});
});


jQuery.noConflict()(function($){
$(document).ready(function() {
	$('ul#filter-sidebar a').click(function() {
		$(this).css('outline','none');
		$('ul#filter-sidebar .current').removeClass('current');
		$(this).parent().addClass('current');
		
		var filterVal = $(this).text().toLowerCase().replace(' ','-');
				
		if(filterVal == 'all') {
			$('ul#portfolio li.hidden').fadeIn('slow').removeClass('hidden');
		} else {
			
			$('ul#portfolio li').each(function() {
				if(!$(this).hasClass(filterVal)) {
					$(this).fadeOut('normal').addClass('hidden');
				} else {
					$(this).fadeIn('slow').removeClass('hidden');
				}
			});
		}

		return false;
	});
});
});

jQuery.noConflict()(function($){
$(document).ready(function() {
            $('#slider').nivoSlider({
                pauseTime:5000,
                pauseOnHover:true,
				captionOpacity:0.9
            });        
    });
});

/***************************************************
			Player Globo.com
***************************************************/	
var Player = {
  loaded: false,

  element: function() {
    return $("#catalogo-palco .player");
  },

  videoId: function() {
    return this.element().data("videoId") || this.element().data("video-id");
  },

  zoneId: function() {
    return Player.element().data("player-zoneid");
  },

  sitePage: function() {
    return this.element().data("playerSitePage") || this.element().data("player-sitePage");
  },

  displayThumbsUpButton: function() {
    return this.element().data("player-displaythumbsupbutton");
  },

  trilhoId: function() {
    return Player.element().attr("data-trilho-id");
  },

  init: function() {
    this.initPlayer();
    this.element().trigger("catalogo.beforePlayerLoaded", this._wmPlayer);
    this._wmPlayer.attachTo(this.element().get(0));
    this.element().trigger("catalogo.afterPlayerLoaded", this._wmPlayer);
    Player.loaded = true;
  },

  initPlayer: function() {
    this._wmPlayer = new WM.Player({
      width: 640,
      height: 360,
      videosIDs: this.videoId(),
      sitePage: this.sitePage(),
      zoneId: this.zoneId(),
      displayThumbsUpButton: this.displayThumbsUpButton(),

      banners: {
        "300x60" : "banner-opec"
      },

      autoPlay: true,
      enableServerSeek: true,
      displayLightButton: true,
      displayStretchButton: true,

      displayPreviewToolTip: true,
      stretchWidth: 940,
      stretchHeight: 529,

      onPlay: function() {
        Player.element().trigger("catalogo.onPlay", Player._wmPlayer);
      },

      onPause: function() {
        Player.element().trigger("catalogo.onPause", Player._wmPlayer);
      },

      onLightOnStart: function() {
        Player.element().trigger("catalogo.onLightOnStart", Player._wmPlayer);
      },

      onLightOffStart: function() {
        Player.element().trigger("catalogo.onLightOffStart", Player._wmPlayer);
      },

      onPlayRelatedClick: function(videoRelatedId) {
        Player.element().trigger("catalogo.onPlayRelatedClick", [Player._wmPlayer, videoRelatedId]);
        return true;
      },

      onShrinkStart: function() {
        Player.element().trigger("catalogo.onShrinkStart", [Player._wmPlayer]);
      },

      onStretchStart: function (data) {
        Player.element().trigger("catalogo.onStretchStart", [Player._wmPlayer]);
      },

      onShrinkComplete: function (data) {
        Player.element().trigger("catalogo.onShrinkComplete", [Player._wmPlayer]);
      },

      onStretchComplete: function (data) {
        Player.element().trigger("catalogo.onStretchComplete", [Player._wmPlayer]);
      },

      onSignedIn: function() {
        Player.element().trigger("catalogo.onSignedIn", Player._wmPlayer);
      },

      complete: function() {
        Player.element().trigger("catalogo.complete", Player._wmPlayer);
      }
    });
  }
};

$(function(){
  Player.init();
});