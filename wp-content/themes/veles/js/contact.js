jQuery.noConflict()(function($){
$(document).ready(function ()
{ 
    $("#ajax-contact-form").submit(function ()
    {
        var str = $(this).serialize(); 
        $.ajax(
        {
            type: "POST",
            url: "http://www.fatecitapetininga.edu.br/wp-content/themes/veles/contact.php",
            data: str,
            success: function (msg)
            {
                $("#note").ajaxComplete(function (event, request, settings)
                {
                    if (msg == 'OK')
                    {
                        result = '<div class="notification_ok">A mensagem foi enviada. Agradecemos por entrar em contato!</div>';
                        $("#fields").hide();
                    }
                    else
                    {
                        result = msg;
                    }
                    $(this).html(result);
                });
            }
        });
        return false;
    });
});
});

jQuery.noConflict()(function($){
$(document).ready(function ()
{ 
    $("#form-requisicao").submit(function ()
    {
        var str = $(this).serialize(); 
        $.ajax(
        {
            type: "POST",
            url: "http://www.fatecitapetininga.edu.br/wp-content/themes/veles/requisicao.php",
            data: str,
            success: function (msg)
            {
                $("#note").ajaxComplete(function (event, request, settings)
                {
                    if (msg == 'OK')
                    {
                        result = '<div class="notification_ok">A requisição foi enviada. Passe na secretaria acadêmica para retirá-la.</div>';
                        $("#fields").hide();
                    }
                    else
                    {
                        result = msg;
                    }
                    $(this).html(result);
                });
            }
        });
        return false;
    });
});
});