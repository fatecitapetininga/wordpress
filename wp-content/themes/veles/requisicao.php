<?php

$ip=$_SERVER['REMOTE_ADDR'];
define("WEBMASTER_EMAIL", 'secretaria.itape@fatec.sp.gov.br'); // destino: Secretaria Acadêmica

error_reporting (E_ALL); 
 
if(!empty($_POST))
{
$_POST = array_map('trim', $_POST); 
$nome = htmlspecialchars($_POST['nome']);
$documento = $_POST['documento'];
$ra = $_POST['ra'];
$turno = $_POST['turno'];
$curso = $_POST['curso'];
$justificativa = htmlspecialchars($_POST['justificativa']);
 
$error = array();
 
 
if(empty($nome))
{
$error[] = 'Por favor, informe o seu nome';
}
 
 
if(empty($ra))
{
$error[] = 'Por favor, informe o seu ra';
}
 
 
if(empty($justificativa) || empty($justificativa{15})) 
{
$error[] = "Envie uma justificativa com pelo menos 15 caracteres";
}
 
if(empty($error)) { 

require("inc/class.phpmailer.php");

// Define os dados do servidor e tipo de conexão
$fatec_email = "fatec@outlook.com";
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail = new PHPMailer();

$mail->CharSet = 'UTF-8';
$mail->IsSMTP(); // Define que a mensagem será SMTP
//$mail->Host = "localhost"; // Endereço do servidor SMTP (caso queira utilizar a autenticação, utilize o host smtp.seudomínio.com.br)
$mail->SMTPAuth = true; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
$mail->Username = $fatec_email; // Usuário do servidor SMTP (endereço de email)
$mail->Password = 'f@tek2012site'; // Senha do servidor SMTP (senha do email usado)
 
// Define o remetente
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->From = $fatec_email; // Seu e-mail
$mail->Sender = $fatec_email; // Seu e-mail
$mail->FromName = "Site - Fatec Itapetininga"; // Seu nome
 
// Define os destinatário(s)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->AddAddress(WEBMASTER_EMAIL);
//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta
 
// Define os dados técnicos da Mensagem
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
//$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)
 
// Define a mensagem (Texto e Assunto)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->Subject  = "Requisição de Documentos - $documento"; // Assunto da mensagem
$mail->Body = "<p><strong>Nome:</strong> $nome </p>".
			  "<p><strong>RA:</strong> $ra / ".			 
			  "<strong>Turno:</strong> $turno / ".
			  "<strong>Curso:</strong> $curso </p>".
			  "<p><strong>Documento Requisitado:</strong> $documento </p>".
			  "<p><strong>Justificativa:</strong> $justificativa </p>".
			  "<p style='font-size: 70%; color: #999;'><em>$ip</p>";
 
// Define os anexos (opcional)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//$mail->AddAttachment("/home/login/documento.pdf", "novo_nome.pdf");  // Insere um anexo
 
// Envia o e-mail
$enviado = $mail->Send();
 
// Limpa os destinatários e os anexos
$mail->ClearAllRecipients();
$mail->ClearAttachments();
 
if($enviado) {
	echo
		"<script type='text/javascript'>		
			alert('A sua solicitação de atestado foi enviada com sucesso! Passe na secretaria para retirar.');
			location.reload(true);
		</script>";
}
 
} else {
	echo '<div class="notification_error">'.implode('<br />', $error).'</div>';
}
}

?>