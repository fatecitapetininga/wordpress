﻿<?php

//Pega o IP
$ip=$_SERVER['REMOTE_ADDR'];
// Pega o destino da mensagem (Hidden Field)
$msgTo = $_POST['setor'];
// Para qual setor?
switch ($msgTo) {
	case 'biblioteca':
		define("WEBMASTER_EMAIL", 'biblioteca.itape@fatec.sp.gov.br'); // destino: Biblioteca;
		break;
	case 'direcao':
		define("WEBMASTER_EMAIL", 'f.itapetininga.dir@centropaulasouza.sp.gov.br'); // destino: Direção;
		break;
	case 'dir_administrativa':
		define("WEBMASTER_EMAIL", 'rh.itape@fatec.sp.gov.br'); // destino: Diretoria Administrativa;
		break;
	case 'estagio':
		define("WEBMASTER_EMAIL", 'estagio.itape@fatec.sp.gov.br'); // destino: Central de Estágio;
		break;
	case 'secretaria':
		define("WEBMASTER_EMAIL", 'secretaria.itape@fatec.sp.gov.br'); // destino: Secretaria Acadêmica;
		break;
	case 'ti':
		define("WEBMASTER_EMAIL", 'f.itapetininga.lab@centropaulasouza.sp.gov.br'); // destino: Departamento de TI;
		break;
	default:
		define("WEBMASTER_EMAIL", 'secretaria.itape@fatec.sp.gov.br'); // destino padrão
		break;
}	
 
error_reporting (E_ALL); 
 
// Realiza as validações de nome, assunto, mensagem... 
if(!empty($_POST)) {
	$_POST = array_map('trim', $_POST); 
	$name = htmlspecialchars($_POST['name']);
	$email = $_POST['email'];
	$subject = htmlspecialchars($_POST['subject']);
	$message = htmlspecialchars($_POST['message']);
	 
	$error = array();
 
if(empty($name)) {
	$error[] = 'Por favor, informe o seu nome.';
}
 
if(empty($email)) {
	$error[] = 'Por favor, informe o seu e-mail.';
} elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) { 
	$error[] = 'O formato do e-mail está incorreto';
}

if(empty($subject)) {
	$error[] = 'Por favor, informe o assunto.';
}
 
if(empty($message) || empty($message{15})) {
	$error[] = "Envie uma mensagem com pelo menos 15 caracteres.";
}

if(empty($error)) { 

require("inc/class.phpmailer.php");

// Define os dados do servidor e tipo de conexão
$fatec_email = "fatec@outlook.com";

// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail = new PHPMailer();
$mail->CharSet = 'UTF-8';
$mail->IsSMTP(); // Define que a mensagem será SMTP
$mail->SMTPAuth = true; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
$mail->Username = $fatec_email; // Usuário do servidor SMTP (endereço de email)
$mail->Password = 'f@tek2012site'; // Senha do servidor SMTP (senha do email usado)
 
// Define o remetente
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->AddReplyTo($email, $name);
$mail->From = $fatec_email; // Seu e-mail
$mail->Sender = $fatec_email; // Seu e-mail
$mail->FromName = "Site - Fatec Itapetininga"; // Seu nome
 
// Define os destinatário(s)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->AddAddress(WEBMASTER_EMAIL);
//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta
 
// Define os dados técnicos da Mensagem
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
//$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)
 
// Define a mensagem (Texto e Assunto)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->Subject  = $subject; // Assunto da mensagem
$mail->Body = "<p><strong>Nome:</strong> $name".
			  "<p><strong>E-mail:</strong> $email".			 
			  "<p><strong>Mensagem:</strong> $message".
			  "<p style='font-size: 70%; color: #999;'><em>$ip</p>";
 
// Define os anexos (opcional)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//$mail->AddAttachment("/home/login/documento.pdf", "novo_nome.pdf");  // Insere um anexo
 
// Envia o e-mail
$enviado = $mail->Send();
 
// Limpa os destinatários e os anexos
$mail->ClearAllRecipients();
$mail->ClearAttachments();
 
// Exibe uma mensagem de resultado
if ($enviado) {
	echo 
		"<script type='text/javascript'>
			alert('Mensagem enviada com sucesso. Agradecemos pelo contato');
			location.reload(true);
		</script>";
} 

} else {
	echo '<div class="notification_error">'.implode('<br />', $error).'</div>';	
	echo $mail;
}
}
 
?> 