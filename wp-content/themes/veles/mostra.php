<?php
	// Template Name: Mostra de Projetos
?>
<?php get_header(); ?>
	<div class="container">
		<div class="span-24">
            <?php if($data['checkbox_google_map'] == true ) { ?>            
            <?php } ?>
            <?php if($data['checkbox_contact_form'] == true ) { ?>
            <div class="span-16 post_form <?php if($data['checkbox_google_map'] == false ) { ?> notopmargin <?php } ?>">
            	<fieldset class="info_fieldset">
                <div id="note"></div>
                <div id="contacts-form">
                <form id="ajax-contact-form" action="#">
                <div class="span-8 form notopmargin">
                  <input class="text" type="text" name="name" value="" placeholder="Name" required /><br />
                </div>
                <div class="span-8 form last notopmargin">
                  <input class="text" type="text" name="email" value="" placeholder="E-mail" required /><br />
                </div>
                <div class="span-16 last formtop">
                    <input class="texts" type="text" name="subject" value="" placeholder="Subject" required /><br />
                </div>
                <div class="span-16 last">
                    <textarea class="text" name="message" rows="5" cols="25" placeholder="Message" required ></textarea><br />
                </div>
                <div class="span-12 last notopmargin">
                    <input class="button" type="submit" name="submit" value="Send message" id="submit_form" required/>
                </div>
                </form>
                </div>
                </fieldset>
            </div>
        
            <?php } ?>
        </div>
        <div class="clear"></div>
	</div>
<?php get_footer(); ?>