<?php

// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<h4 class="collored">Este post é protegido por senha. Informe a senha para visualizar os comentários.</h4>
	<?php
		return;
	}
?>

<!-- You can start editing here. -->
<div class="three-fourth last">
<h4 class="notopmargin uppercase"><?php comments_number('','<span  class="colored">1</span> Comentário:','<span  class="colored">%</span> Comentários:')?></h4>
<?php if ( have_comments() ) : ?>
	
        <ul>
        	<?php wp_list_comments('max_depth=3&callback=mytheme_comment'); ?>     
       </ul>
	
 <?php else : // this is displayed if there are no comments so far ?>
	<?php if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->
	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<h4 class="colored comments-closed">Comments are closed.</h4>
	<?php endif; ?>
<?php endif; ?>
</div>
<div class="clear"></div>
<?php if ( comments_open() ) : ?>
</div>
<div class="">
<div class="span-16 post_form">
<div id="respond">
<h4 class="colored uppercase"><?php comment_form_title( 'Deixe uma resposta:', 'Deixe uma resposta para %s:' ); ?></h4>
<div class="dot-separator margin15"></div>
<!--- replace comment_form();  -->
<?php paginate_comments_links('prev_text=back&next_text=forward'); ?>
<div class="cancel-comment-reply">
	<small><?php cancel_comment_reply_link(); ?></small>
</div>

<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
<p class="blockquote4"Você deve estar <a href="<?php echo wp_login_url( get_permalink() ); ?>">logado</a> para postar um comentário.</p>
<?php else : ?>
<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="contact-form">


<?php if ( is_user_logged_in() ) : ?>
<p class="blockquote4" style="font-size:12px; font-style:normal;"><span  class="strong">Logado como <span  class="colored"><?php echo $user_identity; ?></span>.</span> <a class="button_readmore" style="font-size:10px; padding-top:1px;" href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">LOG OUT</a></p>
<?php else : ?>
	<div class="span-8 form notopmargin">
		<label for="author"><span class="black">Autor:</span></label>
		<input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" class="text" />
    </div>
    
    <div class="span-8 form notopmargin last">
	<label for="email"><span class="black">E-mail</span></label>
	<input type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" class="text"/>
    </div>
<?php endif; ?>

	<div class="span-16  last">
		<label for="comment"><span class="black">Comentário</span></label>
		<textarea id="comment" name="comment" cols="" rows="10" class="text" placeholder="Mensagem"></textarea>
		<div class="span-12 last" style="margin-top:10px;">
        <input name="submit" type="submit" id="submit" class="button"  value="Comentar" />
        </div>
	</div>
		
	
<p><?php comment_id_fields(); ?></p>
<?php do_action('comment_form', $post->ID); ?>
</form>

<?php endif; // If registration required and not logged in ?>

</div>
</div>
<?php endif; // if you delete this the sky will fall on your head ?>