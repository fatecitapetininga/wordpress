<?php
	// Template Name: Fale com a Direção
?>
<?php get_header(); ?>
	<div class="container">
		<div class="span-16 fale-direcao">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 
        <?php the_content(); ?>    
        <?php endwhile;  ?> 
        <?php endif; ?>
            <?php if($data['checkbox_contact_form'] == true ) { ?>
            <div class="post_form notopmargin">
            	<fieldset class="info_fieldset">
                <div id="note"></div>
                <div id="contacts-form">
                <form id="ajax-contact-form" action="javascript:alert('A Mensagem foi enviada!');">
                 <input type="hidden" name="setor" id="setor" value="direcao" />   
                <div class="span-8 notopmargin">
		          <label for="name"><span class="strong">Nome:</span></label>
                  <input class="text" type="text" name="name" id="name" value="" placeholder="Nome" required /><br />
                </div>
                <div class="span-8 notopmargin">
		          <label for="email"><span class="strong">E-mail:</span></label>                
                  <input class="text" type="text" name="email" id="email" value="" placeholder="E-mail" required /><br />
                </div>
                <div class="span-16">
		            <label for="subject"><span class="strong">Assunto:</span></label>
                    <input class="texts" type="text" name="subject" id="subject" value="" placeholder="Assunto" required /><br />
                </div>
                <div class="span-16">
		            <label for="message"><span class="strong">Mensagem:</span></label>
                    <textarea class="text" name="message" id="message" rows="5" cols="25" placeholder="Mensagem" required ></textarea><br />
                </div>
                <div class="span-12">
                    <input class="button" type="submit" name="submit" value="Enviar" id="submit_form" required/>
                </div>
                </form>
                </div>
                </fieldset>
            </div>
            <?php } ?>           
        </div>
        <div class="span-8 skills last">
            <?php get_sidebar(); ?>
        </div>         
        <div class="clear"></div>
	</div>
<?php get_footer(); ?>