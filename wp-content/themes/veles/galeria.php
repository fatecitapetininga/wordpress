<?php
	// Template Name: Galeria
?>
<?php get_header(); ?>
	<div class="container">
    <div class="span-24">
        <? $loop = new WP_Query(array('galeria=true', 'order' => 'DESC')); ?>
		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <h3><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h3>  
        <?php endwhile;  ?>         
    </div>
    <div class="clear"></div>
	</div>
    <div class="clear"></div>
<?php get_footer(); ?>