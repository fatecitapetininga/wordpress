<?php get_header(); ?>

	<div class="container">
    <div class="span-16">
    	<h3> Página não Encontrada! :( </h3>
    	<p> A url informada não existe. A página pode ter sido excluída ou realmente nunca existiu! Tente navegar através do menu principal ou utilize o campo de busca.</ps>
    </div>
    <div class="span-8 skills last">
    	<?php get_sidebar(); ?>
    </div>    
    <div class="clear"></div>
	</div>
    <div class="clear"></div>    	

<?php get_footer(); ?>