    <?php  global $data; 
	?>
    <!--Footer-->
    <div class="footer">
    	<div class="container">
        	<div class="span-9 logo_area">
            	<?php if($data['footer_logo'] == true ) { ?>
            	<a href=""><img src="<?php echo stripslashes($data['footer_logo']); ?>" alt="Logo" /></a>
                <?php } ?>
                <h6><?php echo $data['feel_free']; ?></h6>
                <p><span class="black">Endereço:</span> <?php echo $data['veles_adress']; ?><br/>
                <span class="black">CEP:</span> <?php echo $data['footer_cep']; ?><br/>
		<span class="black">Telefone:</span> <?php echo $data['veles_phone']; ?></p>                
                <div class="separator"></div>
                <?php if($data['Twitter'] == true ) { ?>
                <div class="tweet-icon">
                    <a href="<?php echo $data['twitter_url']; ?>" target="_blank" class='social' title='Twitter'><img src="<?php echo get_template_directory_uri(); ?>/images/1px.png" alt="" width="26px" height="26px" /></a>
                </div>
                <?php } ?>
                <?php if($data['Facebook'] == true ) { ?>
                <div class="facebook-icon">
                    <a href="<?php echo $data['facebook_url']; ?>" target="_blank" class='social' title='Facebook'><img src="<?php echo get_template_directory_uri(); ?>/images/1px.png" alt="" width="26px" height="26px" /></a>
                </div>
                <?php } ?>
                <?php if($data['Google+'] == true ) { ?>
                <div class="google-icon">
                    <a href="<?php echo $data['google_url']; ?>" class='social' title='Google +'><img src="<?php echo get_template_directory_uri(); ?>/images/1px.png" alt="" width="26px" height="26px" /></a>



                </div>



                <?php } ?>



                <?php if($data['Vimeo'] == true ) { ?>



                <div class="vimeo-icon">



                    <a href="<?php echo $data['vimeo_url']; ?>" class='social' title='Vimeo'><img src="<?php echo get_template_directory_uri(); ?>/images/1px.png" alt="" width="26px" height="26px" /></a>



                </div>



                <?php } ?>



                <?php if($data['Dribbble'] == true ) { ?>



                <div class="dribbble-icon">



                    <a href="<?php echo $data['dribbble_url']; ?>" class='social' title='Dribbble'><img src="<?php echo get_template_directory_uri(); ?>/images/1px.png" alt="" width="26px" height="26px" /></a>



                </div>



                <?php } ?>



                <div class="feed-icon">



                    <a href="<?php bloginfo('rss2_url'); ?>" class='social' title='Feed'><img src="<?php echo get_template_directory_uri(); ?>/images/1px.png" alt="" width="26px" height="26px" /></a>



                </div>                



            </div>



            <div class="span-8 posts recent">



            	<?php if (empty($data['footer_block3_text']) & empty($data['footer_block3_title'])){ ?>



                <h5 class="footer_welcome"><span class="colored">Notícias</span> recentes</h5>



                <div class="span-8 separator" style="margin-bottom:0px;"></div>



                <ul style="margin-top:25px;">



                <li><?php wp_get_archives('type=postbypost&limit=5'); ?></li>



                </ul>



                <?php } ?>



                <?php if (($data['footer_block3_text']) || ($data['footer_block3_title'])){ ?>



                <h5 class="footer_welcome"><?php if (empty($data['footer_block3_title'])) { echo "No title here"; }else{ echo $data['footer_block3_title']; }?></h5>



                <div class="span-8 separator"></div>



                <p class="small"><?php echo $data['footer_block3_text']; ?></p>



                <?php } ?>



            </div>



            <div class="span-7 last">



            	<?php if (empty($data['footer_block4_text']) & empty($data['footer_block4_title'])){ ?>



                <h5 class="footer_welcome"><a href="<?php echo $data['twitter_url']; ?>" target="_blank" title="Siga o Sou + Fatec no Twitter!"><span class="black">Últimos</span> <span class="colored">tweets</span></a></h5>



                <div class="span-7 separator"></div>



                <div id="jstwitter" class="tweet">



                </div>



                <?php } ?>



                <?php if (($data['footer_block4_text']) || ($data['footer_block4_title'])){ ?>



                <h5 class="footer_welcome"><?php if (empty($data['footer_block4_title'])) { echo "No title here"; }else{ echo stripslashes($data['footer_block4_title']); }?></h5>



                <div class="span-7 separator"></div>



                <p class="small"><?php echo stripslashes($data['footer_block4_text']); ?></p>



                <?php } ?>		
            </div>
        </div>
    	<div class="clear"></div>
    </div>
    <?php wp_footer(); ?>
    <!--End Footer-->
</body>
<script src="<?php echo get_template_directory_uri(); ?>/js/superfish-menu/superfish.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.prettyPhoto.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/contact.js"></script>
<script type="text/javascript">
/***************************************************
            TWITTER FEED
***************************************************/

jQuery.noConflict()(function($){
$(document).ready(function() {  

      $(".tweet").tweet({
            count: 2,
            username: '<?php echo $data['twitter_feed']; ?>',
            loading_text: "Carregando Twitter..."      
        });
});
});
</script>
</html>