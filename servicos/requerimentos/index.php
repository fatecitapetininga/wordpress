<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Fatec Itapetininga</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
  </head>
  
  <body>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Secretaria</a> 
        </div>
      </nav>   
      <!-- /. NAV TOP  -->
      <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
          <ul class="nav" id="main-menu">
            <li class="text-center">
              <img src="assets/img/logo-fatec.png" class="user-image img-responsive"/>
            </li>
            <li>
              <a class="active-menu" href="index.php"><i class="fa fa-desktop fa-3x"></i> Início</a>
            </li>
            <li>
              <a  href="consulta.php"><i class="fa fa-table fa-3x"></i> Consulta</a>
            </li> 
          </ul>
        </div>
        
      </nav>  
      <!-- /. NAV SIDE  -->
      <div id="page-wrapper" >
        
        <div class="col-md-12">
          <h2>Painel de Informações</h2>   
          <h5>Seja Bem Vindo!!!</h5>      
        </div>
        <!-- /. ROW  -->
        <hr />                
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">           
            <div class="panel panel-back noti-box">
              <span class="icon-box bg-color-blue">
                <i class="fa fa-warning"></i>
              </span>
              <div class="text-box" >
                <p class="main-text">Atenção</p>
                <p class="text-mutedcolor-bottom-txt">Este sistema foi desenvolvido para que você possa conferir os seus requerimentos.<br>Serão exibidos apenas requerimentos solicitados a partir de 29/04/2015. </p>
                <hr />
                <p class="text-muted">
                  <span class="text-muted color-bottom-txt"><i class="fa fa-edit"></i>
                    Utilize o painel de consultas para encontrar o seu requerimento utilizando o número do protocolo.
                  </span>
                </p>
              </div>
            </div>
          </div>
          
        </div>
        <!-- /. PAGE INNER  -->
      </div>
      <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- MORRIS CHART SCRIPTS -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
    <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
  </body>
  
</html>
