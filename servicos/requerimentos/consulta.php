<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Fatec Itapetininga</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    
  </head>
  <body>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Secretaria</a> 
        </div>
      </nav>   
      <!-- /. NAV TOP  -->
      <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
          <ul class="nav" id="main-menu">
            <li class="text-center">
              <img src="assets/img/logo-fatec.png" class="user-image img-responsive"/>
            </li>
            <li>
              <a  href="index.php"><i class="fa fa-desktop fa-3x"></i> Início</a>
            </li>
            <li>
              <a class="active-menu" href="consulta.php"><i class="fa fa-table fa-3x"></i> Consulta</a>
            </li>
          </ul>
        </div>
        
      </nav>  
      <!-- /. NAV SIDE  -->
      <div id="page-wrapper" >
        <div id="page-inner">
          <div class="row">
            <div class="col-md-12">
              <h2>Consulta de Requerimentos Analisados</h2>   
            </div>
          </div>
          <!-- /. ROW  -->
          <hr />
          
          <div class="row">
            <div class="col-md-12">
              <!-- Advanced Tables -->
              <div class="panel panel-default">
                <div class="panel-heading">
                  Requerimentos
                </div>
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="requerimentos">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nº Protocolo</th>
                          <th>Data do Pedido</th>
                          <th>Parecer</th>
                          <th>Observação</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="5" class="dataTables_empty">Carregando dados do servidor...</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  
                </div>
              </div>
              <!--End Advanced Tables -->
            </div>
          </div>
          <!-- /. ROW  -->
          
          
        </div>
        <!-- /. PAGE INNER  -->
      </div>
      <!-- /. PAGE WRAPPER  -->
      <!-- /. WRAPPER  -->
      
      <!-- Modal -->
      <div class="modal fade" id="observacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Observações do Requerimento</h4>
            </div>
              <div class="modal-body">
                <div class="form-group">
                <label>Observação</label>
                <textarea rows="4" cols="50" id="observacao-txt" name="observacao-txt" readonly class="form-control" type="text" placeholder=""></textarea>
                
              </div>
              <strong><a href="http://www.centropaulasouza.sp.gov.br/quem-somos/departamentos/cesu/regulamento-de-graduacao.pdf" target="_blank">Regulamento de Graduação</a></strong>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
              </div>
          </div>
        </div>
      </div>
      
      <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
      <!-- JQUERY SCRIPTS -->
      <script src="assets/js/jquery-1.11.2.min.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
      <script src="assets/js/bootstrap.min.js"></script>
      <!-- METISMENU SCRIPTS -->
      <script src="assets/js/jquery.metisMenu.js"></script>
      <!-- DATA TABLE SCRIPTS -->
      <script src="assets/js/dataTables/jquery.dataTables.js"></script>
      <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
      <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
          var table = $('#requerimentos').dataTable( {
            "Processing": true,
            "ServerSide": true,
            "sAjaxSource": "control/datasearch.php",
            "order": [ 0, "desc" ],
            "columnDefs": [
              {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
              }, 
              {
                "targets": -1,
                "data": null,
                "defaultContent": "<button class='btn btn-primary btn-xs'>Abrir</button>"
              } 
            ],
            "language": {
              "lengthMenu": "Exibindo _MENU_ registros por página",
              "zeroRecords": "Nenhum registro encontrado",
              "info": "Exibindo página _PAGE_ de _PAGES_",
              "infoEmpty": "Sem registros",
              "infoFiltered": "(Filtrado de _MAX_ registros totais)",
              "sSearch": "Busca: ",
              "oPaginate": {
                "sPrevious": "Anterior",
                "sNext": "Próximo"
              }
            },
          } );
          
          $('#requerimentos tbody').on( 'click', 'button', function () {
            document.getElementById("observacao-txt").value = "Carregando...";
            $("#observacao").modal();
            var $row = $(this).closest("tr"),
                $tds = $row.find("td");
            var data = [];

            $.each($tds, function() {
              data.push($(this).text());
            });  
            
            $.ajax({
              type : 'POST',
              url : 'control/load_observacao.php',
              data: {
                protocolo : data[0]
              },
              success : function(res){
                document.getElementById("observacao-txt").value = res;
              },
            });
                        
          } );
          
        } );
      </script>
      <!-- CUSTOM SCRIPTS -->
      <script src="assets/js/custom.js"></script>
      
      
    </body>
    </html>
