<?php
 
  include_once('db_config.php');
  
  $protocolo = $_POST['protocolo'];
  
  $stmt = $con->prepare('SELECT id FROM parecer WHERE numero=?;');
  $stmt->bindParam(1,$protocolo);
  $stmt->execute();
  
  $id=0;
  
  foreach($stmt as $i){
    $id = $i["id"];
  }
  
  $stmt = $con->prepare('SELECT motivo FROM parecer WHERE id=?;');
  $stmt->bindParam(1,$id);
  $stmt->execute();
  
  $data=""; 
  
  foreach($stmt as $i){
    $data = $i["motivo"];
  }
  
  echo $data;
  
?>